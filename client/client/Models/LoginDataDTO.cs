﻿using System;
using System.Collections.Generic;
using System.Text;

namespace client.Models
{
    public class LoginDataDTO
    {
        public string username { get; set;}
        public string password { get; set;}
        public bool login { get; set; }
    }
}
