﻿using System;
using System.Collections.Generic;
using System.Text;

namespace client.Models
{
    public enum MenuItemType
    {
        Browse,
        About,
        myOffers,
        offers,
        stats,
        clients
    }
    public class HomeMenuItem
    {
        public MenuItemType Id { get; set; }

        public string Title { get; set; }
    }
}
