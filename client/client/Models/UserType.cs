﻿using System;
using System.Collections.Generic;
using System.Text;

namespace client.Models
{
    public enum UserType : int
    {
        manager,
        carrier,
        user
    }
}
