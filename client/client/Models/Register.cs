﻿using System;
using System.Collections.Generic;
using System.Text;

namespace client.Models
{
    public class Register
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
        public string Name { get; set; }
    }
}
