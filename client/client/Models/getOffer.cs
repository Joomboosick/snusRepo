﻿using System;
using System.Collections.Generic;
using System.Text;

namespace client.Models
{
    public class getOffer
    {

        public int id { get; set; }
        public string clientUserName { get; set; }
        public string adress { get; set; }
        public string attention { get; set; }
        public string deliveryInfo { get; set; }
        public string Carrier { get; set; }
        public int? status { get; set; }
    }
}
