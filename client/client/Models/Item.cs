﻿using SQLite;
using System;
using Xamarin.Forms;

namespace client.Models
{
    public class Item
    {
        public string GUId { get; set; }
        public string id { get; set; }
        public string posName { get; set; }
        public string CompanyName {get;set;}
        public string describ { get; set; }
        public string count { get; set; }
        public string price { get; set; }
        public string optPrice { get; set; }
        public string imgUrl { get; set; }
        [Ignore]
        public Command OnItemLongTap { get; set; }
        [Ignore]
        public Command OnTap { get; set; }


    }
}