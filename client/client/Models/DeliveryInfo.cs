﻿using System;
using System.Collections.Generic;
using System.Text;

namespace client.Models
{
    public class DeliveryInfo
    {
        public Item[] Items { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string FullPrice { get; set; }
    }
}
