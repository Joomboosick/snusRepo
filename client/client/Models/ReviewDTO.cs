﻿using System;
using System.Collections.Generic;
using System.Text;

namespace client.Models
{
    public class ReviewDTO
    {
        public int? PosId { get; set; }
        public string comment { get; set; }
        public string clientName { get; set; }
        public int? stars { get; set; }
        public float? Avg { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public string starsUrl { get; set; }

        public ReviewDTO(int? _posId = null, string _comment = null, string _clientName = null, int? _stars = null, float? _avg = null)
        {
            PosId = _posId;
            comment = _comment;
            clientName = _clientName;
            stars = _stars;
            Avg = _avg;
        }

    }
}
