﻿using SQLite;

namespace client.Models
{
    public class LocalLoginData
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public int UserType { get; set; }
    }
}
