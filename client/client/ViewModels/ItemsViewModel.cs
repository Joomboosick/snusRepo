﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using DLToolkit.Forms.Controls;
using client.Models;
using client.Views;
using Rg.Plugins.Popup.Services;

namespace client.ViewModels
{
    public class ItemsViewModel : BaseViewModel
    {
        public FlowObservableCollection<Item> Items { get; set; }
        public Command LoadItemsCommand { get; set; }
        private INavigation Navigation;
        public ItemsViewModel(INavigation _Navigation)
        {
            Navigation = _Navigation;
            Title = "Позиции";
            Items = new FlowObservableCollection<Item>();
            LoadItemsCommand = new Command(() => ExecuteLoadItemsCommand());
            MessagingCenter.Subscribe<CartPage, Item>(this, "AddItem", async (obj, item) =>
            {
                var newItem = item as Item;
                Items.Add(newItem);
                await DataStore.AddItemAsync(newItem);
            });
        }

       

        public void FirstLoad()
        {
            ExecuteLoadItemsCommand();
            MessagingCenter.Send<Page>(new Page(), "PlayServiceCheck");
        }
        async void OnItemLongTaped( Item item)
        {
            item.posName = item.posName.Replace("\n", "");
            await PopupNavigation.Instance.PushAsync(new ItemPopupPage(item));
        }
        async void OnItemSelected(Item item)
        {
            if (IsBusy)
                return;
            if (!IsBusy)
            {
                try
                {
                    var newPage = new ItemDetailPage(new ItemDetailViewModel(item));
                    foreach (var navStack in Navigation.NavigationStack)
                    {
                        if (navStack.Title == newPage.Title)
                            return;
                    }
                    IsBusy = true;
                    item.posName = item.posName.Replace("\n", "");
                    await Navigation.PushAsync(newPage);
                }
                finally
                {
                    IsBusy = false;
                }
                
            }
        }
        void ExecuteLoadItemsCommand()
        {
            IsBusy = true;

            try
            {
                Items.Clear();
                Services.MockDataStore dataStore = new Services.MockDataStore();
                var items = dataStore.GetItemsFromServer();
                char[] posNmae;
                string newPosName = "";
                foreach (var item in items)
                {
                    posNmae = item.posName.ToCharArray();
                    newPosName = "";
                    for (var i = 1; i < posNmae.Length+1; i++)
                    {
                        if(i%16 == 0)
                        {
                            newPosName += '\n';
                        }
                        newPosName += posNmae[i-1];
                    }
                    item.posName = newPosName;
                    item.OnItemLongTap = new Command(() => OnItemLongTaped(item));
                    item.OnTap = new Command(() => OnItemSelected(item));
                    Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}