﻿using System;

using client.Models;

namespace client.ViewModels
{
    public class ItemDetailViewModel : BaseViewModel
    {
        public Item Item { get; set; }
        public ItemDetailViewModel(Item item = null)
        {
            Title = item?.posName;
            Item = item;
        }
    }
}
