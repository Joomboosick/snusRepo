﻿using client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace client.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegisterPage : ContentPage
    {
        public RegisterPage()
        {
            InitializeComponent();
        }
        public string GetHash(string input)
        {
            var md5 = System.Security.Cryptography.MD5.Create();
            var hash = md5.ComputeHash(Encoding.UTF8.GetBytes(input));

            return Convert.ToBase64String(hash);
        }
        private void RegisterButton_Clicked(object sender, EventArgs e)
        {
            string strPattern = @"^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$";
            if (!Regex.IsMatch(phoneEntry.Text,strPattern,RegexOptions.IgnoreCase))
            {
                DisplayAlert("Ошибка!", "Введите номер в формате +79000000000", "Ok");
                return;
            }
            if(string.IsNullOrEmpty(loginEntry.Text) || string.IsNullOrEmpty(passwordEntry.Text) || string.IsNullOrEmpty(phoneEntry.Text) || string.IsNullOrEmpty(nameEntry.Text))
            {
                DisplayAlert("Ошибка!", "Заполните все поля", "Ok");
                return;
            }
            Register register = new Register
            {
                Login = loginEntry.Text,
                Password = GetHash(passwordEntry.Text),
                Phone = phoneEntry.Text,
                Name = nameEntry.Text
            };
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(register);
            Services.HttpClientMy httpClient = new Services.HttpClientMy();
            json = httpClient.HttpPost("register.php", json, null);
            switch (json)
            {
                case "phone":
                    DisplayAlert("Ошибка!", "Пользователь с таким номером телефона уже существует", "Ok");
                    return;
                case "username":
                    DisplayAlert("Ошибка!", "Пользователь с таким логином уже существует", "Ok");
                    return;   
                case "ok":
                    login();
                    break;
            }
        }
        private void login()
        {
            Services.HttpClientMy http = new Services.HttpClientMy();
            string login = loginEntry.Text;
            string password = passwordEntry.Text;
            var logindata = new LoginDataDTO { username = login, password = password, login = true };

            var response = http.HttpPost("auth.php", "", logindata);

            logindata.login = false;
            if (response == "ERROR")
            {
                DisplayAlert("Непредвиденная ошибка", "повторите попытку", "Ok");
                return;
            }

            if (response == "access denied!")
            {
                Xamarin.Essentials.Vibration.Vibrate(200);
                logpassError.IsVisible = true;
                return;
            }

            switch (int.Parse(response))
            {
                case (int)UserType.manager:
                    saveLoginData(logindata, 0);
                    break;
                case (int)UserType.carrier:
                    saveLoginData(logindata, 1);

                    break;
                case (int)UserType.user:
                    saveLoginData(logindata, 2);

                    break;

                default:
                    break;
            }
            Application.Current.MainPage = new MainPage();
            Navigation.PopModalAsync();


        }
        private void saveLoginData(LoginDataDTO logData, int userType)
        {
            MessagingCenter.Send<Page>(this, "RefreshToken");
            try
            {
                App.PropDatabase.DeleteAccAsync();
                App.Current.Properties.Remove("HMItemList");
            }
            catch { }
            App.PropDatabase.SaveAccAsync(new LocalLoginData { ID = 0, Login = logData.username, Password = logData.password, UserType = userType });
            SetPriveleges(userType);
        }
        private void SetPriveleges(int usertype)
        {
            List<HomeMenuItem> HMItemList;
            switch (usertype)
            {
                case (int)UserType.manager:
                    HMItemList = new List<HomeMenuItem>
                    {
                        new HomeMenuItem {Id = MenuItemType.Browse, Title="Позиции" },
                        new HomeMenuItem {Id = MenuItemType.myOffers, Title="Мои заказы" },
                        new HomeMenuItem {Id = MenuItemType.offers, Title="Заказы" },
                        new HomeMenuItem {Id = MenuItemType.stats, Title="Статистика" },
                        new HomeMenuItem {Id = MenuItemType.clients, Title="Клиенты" },
                        new HomeMenuItem {Id = MenuItemType.About, Title="О нас" }
                    };
                    break;
                case (int)UserType.carrier:
                    HMItemList = new List<HomeMenuItem>
                    {
                        new HomeMenuItem {Id = MenuItemType.Browse, Title="Позиции" },
                        new HomeMenuItem {Id = MenuItemType.myOffers, Title="Мои заказы" },
                        new HomeMenuItem {Id = MenuItemType.offers, Title="Заказы" },
                        new HomeMenuItem {Id = MenuItemType.stats, Title="Статистика" },
                        new HomeMenuItem {Id = MenuItemType.About, Title="О нас" }
                    };
                    break;
                case (int)UserType.user:
                    HMItemList = new List<HomeMenuItem>
                    {
                        new HomeMenuItem {Id = MenuItemType.Browse, Title="Позиции" },
                        new HomeMenuItem {Id = MenuItemType.myOffers, Title="Мои заказы" },
                        new HomeMenuItem {Id = MenuItemType.About, Title="О нас" }
                    };
                    break;
                default:
                    HMItemList = new List<HomeMenuItem>
                    {
                        new HomeMenuItem {Id = MenuItemType.Browse, Title="Позиции" },
                        new HomeMenuItem {Id = MenuItemType.myOffers, Title="Мои заказы" },
                        new HomeMenuItem {Id = MenuItemType.About, Title="О нас" }
                    };
                    break;
            }
            App.Current.Properties.Add("HMItemList", HMItemList);
        }
    }
}