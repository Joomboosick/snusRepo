﻿using client.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;
using System.Threading.Tasks;

namespace client.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MenuPage : ContentPage
    {
        MainPage RootPage { get => Application.Current.MainPage as MainPage; }
        List<HomeMenuItem> menuItems;
        
        public MenuPage()
        {
            InitializeComponent();

            
            
            menuItems = App.Current.Properties["HMItemList"] as List<HomeMenuItem>;
            
            int menuItem = 0;


            ListViewMenu.ItemsSource = menuItems;
            if (!App.Current.Properties.ContainsKey("SelectedMenuItem"))
                MessagingCenter.Send<Page>(new Page(), "ExtrasCheck");

            if (App.Current.Properties.ContainsKey("SelectedMenuItem"))
                menuItem = (int)App.Current.Properties["SelectedMenuItem"];
            ListViewMenu.SelectedItem = menuItems[menuItem];

            ListViewMenu.ItemSelected += async (sender, e) =>
            {
                
                if (e.SelectedItem == null)
                    return;

                var id = (int)((HomeMenuItem)e.SelectedItem).Id;
                await RootPage.NavigateFromMenu(id);
            };



        }

        private bool isLock = false;
        private void ButtonLogout_Clicked(object sender, EventArgs e)
        {
            if (!isLock)
                LogoutConfirm();
        }
        private async Task LogoutConfirm()
        {
            isLock = true;
            Vibration.Vibrate(250);
            var answer = await DisplayAlert("Выход из аккаунта", "Вы действительно хотите выйти?", "Да", "Нет");
            if (answer)
            {
                App.PropDatabase.DeleteAccAsync();
                App.Current.MainPage = new NavigationPage(new LoginPage());
            }
            isLock = false;
        }
    }
}