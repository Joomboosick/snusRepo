﻿using client.Models;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using client.Services;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Collections.Generic;

namespace client.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyOffersPage : ContentPage
    {
        public ObservableCollection<Offer> Items { get; set; }
        bool IsBusy = false;
        public MyOffersPage()
        {
            InitializeComponent();
            Items = new ObservableCollection<Offer>();
            BindingContext = this;
            Command command = new Command(async () => await RefreshPage());
            MessagingCenter.Subscribe<Page>(this, 
                "RefreshOffersPage", 
                async (sender) => { await RefreshPage(); IsBusy = false; }
                );
            MyRefreshView.Command = command;
            GetItemsFromServer();
        }

        private async Task RefreshPage()
        {
            MyRefreshView.IsRefreshing = true;
            try
            {
                GetItemsFromServer();
            }
            finally 
            {

                MyRefreshView.IsRefreshing = false;
            }
        }

        private void GetItemsFromServer()
        {
            // need2 rebuild xaml page set status and carrier visible for user
            Items.Clear();
            HttpClientMy httpClient = new HttpClientMy();
            var json = httpClient.HttpPost("getDelivery.php", "{\"my\":\"true\"}", (LoginDataDTO)App.Current.Properties["LoginData"]);
            if (!string.IsNullOrEmpty(json))
            {
                var getOffers = Newtonsoft.Json.JsonConvert.DeserializeObject<List<getOffer>>(json);
                foreach (var item in getOffers)
                {
                    var dInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<DeliveryInfo>(item.deliveryInfo);
                    Items.Add(new Offer
                    {
                        id = item.id,
                        adress = item.adress,
                        attention = item.attention,
                        Carrier = item.Carrier,
                        clientUserName = item.clientUserName,
                        deliveryInfo = dInfo,
                        status = item.status
                    });
                }
            }
        }

        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            if (IsBusy)
                return;
            else
            {
                try
                {
                    IsBusy = true;
                    var layout = (BindableObject)sender;
                    var item = (Offer)layout.BindingContext;
                    await Navigation.PushModalAsync(new NavigationPage(new MyOfferDetailPage(item)));
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }
    }
}
