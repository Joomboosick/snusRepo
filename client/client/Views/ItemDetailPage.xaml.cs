﻿using System;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using client.Models;
using client.ViewModels;
using System.Threading.Tasks;

namespace client.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class ItemDetailPage : ContentPage
    {
        readonly ItemDetailViewModel viewModel;
        readonly Services.HttpClientMy http;
        public ItemDetailPage(ItemDetailViewModel viewModel)
        {
            InitializeComponent();
            http = new Services.HttpClientMy();
            var json = http.HttpPost(
                "getReview.php", 
                Newtonsoft.Json.JsonConvert.SerializeObject(new ReviewDTO(_posId: int.Parse(viewModel.Item.id), _avg: 1.0f))
                );
            try
            {
                var avgRating = Newtonsoft.Json.JsonConvert.DeserializeObject<ReviewDTO>(json).Avg;
                switch (avgRating)
                {
                    case 1f:
                        RatingImage.Source = "http://joomboosick.ddns.net/ratingIMG/rating10.png";
                        break;
                    case 1.5f:
                        RatingImage.Source = "http://joomboosick.ddns.net/ratingIMG/rating15.png";
                        break;
                    case 2f:
                        RatingImage.Source = "http://joomboosick.ddns.net/ratingIMG/rating20.png";
                        break;
                    case 2.5f:
                        RatingImage.Source = "http://joomboosick.ddns.net/ratingIMG/rating25.png";
                        break;
                    case 3f:
                        RatingImage.Source = "http://joomboosick.ddns.net/ratingIMG/rating30.png";
                        break;
                    case 3.5f:
                        RatingImage.Source = "http://joomboosick.ddns.net/ratingIMG/rating35.png";
                        break;
                    case 4f:
                        RatingImage.Source = "http://joomboosick.ddns.net/ratingIMG/rating40.png";
                        break;
                    case 4.5f:
                        RatingImage.Source = "http://joomboosick.ddns.net/ratingIMG/rating45.png";
                        break;
                    case 5f:
                        RatingImage.Source = "http://joomboosick.ddns.net/ratingIMG/rating50.png";
                        break;
                }
            }
            catch
            {
                        RatingImage.Source = "http://joomboosick.ddns.net/ratingIMG/rating00.png";
            }

            BindingContext = this.viewModel = viewModel;
        }

        

        private async void OnButton_Clicked(object sender, EventArgs e)
        {
            await AddToCart();
        }

        private async Task AddToCart()
        {
            string result = await DisplayPromptAsync("Сколько позиций добавить в корзину", "Количество", initialValue: "1", maxLength: 2, keyboard: Keyboard.Numeric);
            if (string.IsNullOrEmpty(result))
                return;
            int j;
            try
            {
                j = int.Parse(result);
            }
            catch (Exception)
            {
                await DisplayAlert("Ошибка", "Поддерживаются только цифры до 99", "ok");
                return;
            }
            for (int i = 0; i < j; i++)
            {
                App.CartDatabase.SaveItemAsync(viewModel.Item);
            }
            Xamarin.Essentials.Vibration.Vibrate(150);
        }

        private async void ReviewExpandBtn_Clicked(object sender, EventArgs e)
        {
           await Navigation.PushModalAsync(new NavigationPage( new ReviewPage(int.Parse(viewModel.Item.id))));
        }
    }
}