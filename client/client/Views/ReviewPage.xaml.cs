﻿using client.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace client.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ReviewPage : ContentPage
    {
        public ObservableCollection<ReviewDTO> Items { get; set; }
        public ReviewPage(int id)
        {
            InitializeComponent();
            Items = new ObservableCollection<ReviewDTO>();
            Title = "Отзывы";
            BindingContext = this;
            GetReviews(id);
        }

        private void GetReviews(int id)
        {
            Items.Clear();
            var http = new Services.HttpClientMy();
            var json = http.HttpPost(
                "getReview.php",
                Newtonsoft.Json.JsonConvert.SerializeObject(new ReviewDTO(id))
                );
            List<ReviewDTO> reviews = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ReviewDTO>>(json);
            foreach (var item in reviews)
            {
                item.starsUrl = GetStarsUrl(item.stars);
                Items.Add(item);
            }
        }
        private string GetStarsUrl(int? rating)
        {
            switch (rating)
            {
                case 1:
                    return "http://joomboosick.ddns.net/ratingIMG/rating10.png";
                case 2:
                    return "http://joomboosick.ddns.net/ratingIMG/rating20.png";
                case 3:
                    return "http://joomboosick.ddns.net/ratingIMG/rating30.png";
                case 4:
                    return "http://joomboosick.ddns.net/ratingIMG/rating40.png";
                case 5:
                    return "http://joomboosick.ddns.net/ratingIMG/rating50.png";
                default:
                    return "http://joomboosick.ddns.net/ratingIMG/rating00.png";
            }
        }
    }
}