﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using client.Models;
using client.Views;
using client.ViewModels;

namespace client.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class ItemsPage : ContentPage
    {
        readonly ItemsViewModel viewModel;

        public ItemsPage()
        {
            InitializeComponent();
            

            MessagingCenter.Subscribe<Page>(
                this,
                "googlePlaySvcError",
                (sender) => {
                    DisplayAlert("Ошибка", "Сервисы google play не активны\n" +
      "вы будете получать уведомления только пока приложение работает", "Ок");
                });
            viewModel = new ItemsViewModel(Navigation);
            viewModel.FirstLoad();
            BindingContext = viewModel;
            Xamarin.Essentials.DeviceDisplay.MainDisplayInfoChanged += DeviceDisplay_MainDisplayInfoChanged;
        }

        private void DeviceDisplay_MainDisplayInfoChanged(object sender, Xamarin.Essentials.DisplayInfoChangedEventArgs e)
        {
            switch (e.DisplayInfo.Orientation)
            {
                case Xamarin.Essentials.DisplayOrientation.Portrait:
                    ItemsCollectionView.FlowColumnCount = 2;
                    break;
                case Xamarin.Essentials.DisplayOrientation.Landscape:
                    ItemsCollectionView.FlowColumnCount = 3;

                    break;
                default:
                    break;
            }
        }

        

        async void AddItem_Clicked(object sender, EventArgs e)
        {
            if (IsBusy)
                return;
            if (!IsBusy)
            {
                IsBusy = true;
                try
                {
                await Navigation.PushModalAsync(new NavigationPage(new CartPage()));
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.Items.Count == 0)
                viewModel.IsBusy = true;
            

        }
    }
}