﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using client.Models;
using client.Services;
using Xamarin.Essentials;

namespace client.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        public string login = "";
        public string password = "";
        HttpClientMy http;
        public LoginPage()
        {
            InitializeComponent();
        }
    
        public string GetHash(string input)
        {
            var md5 = System.Security.Cryptography.MD5.Create();
            var hash = md5.ComputeHash(Encoding.UTF8.GetBytes(input));

            return Convert.ToBase64String(hash);
        }
        private void LoginButton_Clicked(object sender, EventArgs e)
        {
            http = new HttpClientMy();
            login = loginEntry.Text;
            password = passwordEntry.Text;
            var logindata = new LoginDataDTO { username = login,password = GetHash(password),login = true};

            var response =  http.HttpPost("auth.php", "",logindata);
            
            logindata.login = false;
            if (response == "ERROR")
            {
                DisplayAlert("Непредвиденная ошибка", "повторите попытку", "Ok");
                return;
            }

            if (response == "access denied!")
            {
                Vibration.Vibrate(200);
                logpassError.IsVisible = true;
                return;
            }

            switch (int.Parse(response))
            {
                case (int)UserType.manager:
                    saveLoginData(logindata, 0);
                    break;
                case (int)UserType.carrier:
                    saveLoginData(logindata, 1);

                    break;
                case (int)UserType.user:
                    saveLoginData(logindata, 2);

                    break;

                default:
                    break;
            }
            Application.Current.MainPage = new MainPage();
        }
        private async void RegisterButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new NavigationPage(new RegisterPage()));
        }
        private void saveLoginData(LoginDataDTO logData, int userType)
        {
            try
            { 
                App.PropDatabase.DeleteAccAsync();
                App.Current.Properties.Remove("LoginData");
                App.Current.Properties.Add("LoginData", logData);
                App.Current.Properties.Remove("HMItemList");
            }
            catch{ }
            MessagingCenter.Send<Page>(this, "RefreshToken");
            App.PropDatabase.SaveAccAsync(new LocalLoginData { ID = 0, Login = logData.username, Password = logData.password, UserType=userType });
            SetPriveleges(userType);
        }
        private void SetPriveleges(int usertype)
        {
            List<HomeMenuItem> HMItemList;
            switch (usertype)
            {
                case (int)UserType.manager:
                    HMItemList = new List<HomeMenuItem>
                    {
                        new HomeMenuItem {Id = MenuItemType.Browse, Title="Позиции" },
                        new HomeMenuItem {Id = MenuItemType.myOffers, Title="Мои заказы" },
                        new HomeMenuItem {Id = MenuItemType.offers, Title="Заказы" },
                        new HomeMenuItem {Id = MenuItemType.stats, Title="Статистика" },
                        new HomeMenuItem {Id = MenuItemType.clients, Title="Клиенты" },
                        new HomeMenuItem {Id = MenuItemType.About, Title="О нас" }
                    };
                    break;
                case (int)UserType.carrier:
                    HMItemList = new List<HomeMenuItem>
                    {
                        new HomeMenuItem {Id = MenuItemType.Browse, Title="Позиции" },
                        new HomeMenuItem {Id = MenuItemType.myOffers, Title="Мои заказы" },
                        new HomeMenuItem {Id = MenuItemType.offers, Title="Заказы" },
                        new HomeMenuItem {Id = MenuItemType.stats, Title="Статистика" },
                        new HomeMenuItem {Id = MenuItemType.About, Title="О нас" }
                    };
                    break;
                case (int)UserType.user:
                    HMItemList = new List<HomeMenuItem>
                    {
                        new HomeMenuItem {Id = MenuItemType.Browse, Title="Позиции" },
                        new HomeMenuItem {Id = MenuItemType.myOffers, Title="Мои заказы" },
                        new HomeMenuItem {Id = MenuItemType.About, Title="О нас" }
                    };
                    break;
                default:
                    HMItemList = new List<HomeMenuItem>
                    {
                        new HomeMenuItem {Id = MenuItemType.Browse, Title="Позиции" },
                        new HomeMenuItem {Id = MenuItemType.myOffers, Title="Мои заказы" },
                        new HomeMenuItem {Id = MenuItemType.About, Title="О нас" }
                    };
                    break;
            }
            App.Current.Properties.Add("HMItemList", HMItemList);
        }
    }
}