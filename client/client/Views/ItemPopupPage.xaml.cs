﻿using Rg.Plugins.Popup.Services;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using client.Models;

namespace client.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ItemPopupPage : PopupPage
    {
        public Item Item { get; set; }

        public ItemPopupPage(Item _item)
        {
            Item = _item;
            InitializeComponent();
            var http = new Services.HttpClientMy();
            var json = http.HttpPost(
                "getReview.php",
                Newtonsoft.Json.JsonConvert.SerializeObject(new ReviewDTO(_posId: int.Parse(_item.id), _avg: 1.0f))
                );
            try
            {
                var avgRating = Newtonsoft.Json.JsonConvert.DeserializeObject<ReviewDTO>(json).Avg;
                switch (avgRating)
                {
                    case 1f:
                        RatingImage.Source = "http://joomboosick.ddns.net/ratingIMG/rating10.png";
                        break;
                    case 1.5f:
                        RatingImage.Source = "http://joomboosick.ddns.net/ratingIMG/rating15.png";
                        break;
                    case 2f:
                        RatingImage.Source = "http://joomboosick.ddns.net/ratingIMG/rating20.png";
                        break;
                    case 2.5f:
                        RatingImage.Source = "http://joomboosick.ddns.net/ratingIMG/rating25.png";
                        break;
                    case 3f:
                        RatingImage.Source = "http://joomboosick.ddns.net/ratingIMG/rating30.png";
                        break;
                    case 3.5f:
                        RatingImage.Source = "http://joomboosick.ddns.net/ratingIMG/rating35.png";
                        break;
                    case 4f:
                        RatingImage.Source = "http://joomboosick.ddns.net/ratingIMG/rating40.png";
                        break;
                    case 4.5f:
                        RatingImage.Source = "http://joomboosick.ddns.net/ratingIMG/rating45.png";
                        break;
                    case 5f:
                        RatingImage.Source = "http://joomboosick.ddns.net/ratingIMG/rating50.png";
                        break;
                }
            }
            catch
            {
                RatingImage.Source = "http://joomboosick.ddns.net/ratingIMG/rating00.png";
            }
            BindingContext = this;
        }
        private async void OnClose(object sender, EventArgs e)
        {
            await PopupNavigation.Instance.PopAsync();
        }
        protected override Task OnAppearingAnimationEndAsync()
        {
            return Content.FadeTo(1);
            
        }
        private async Task AddToCartClicked()
        {
            string result = await DisplayPromptAsync("Сколько позиций добавить в корзину", "Количество", initialValue: "1", maxLength: 2, keyboard: Keyboard.Numeric);
            if (string.IsNullOrEmpty(result))
            {
                IsBusy = false;
                return;
            }

            var j = 1;
            try
            {
                j = int.Parse(result);
            }
            catch (Exception)
            {
                await DisplayAlert("Ошибка", "Поддерживаются только цифры до 99", "ok");
                IsBusy = false;
                return;
            }
            for (int i = 0; i < j; i++)
            {
                App.CartDatabase.SaveItemAsync(Item);
            }
            Xamarin.Essentials.Vibration.Vibrate(150);
            IsBusy = false;

        }

        protected override Task OnDisappearingAnimationBeginAsync()
        {
            return Content.FadeTo(0.5);
        }
        private bool  IsBusy = false;
        private async void AddToCartButton_Clicked(object sender, EventArgs e)
        {
            if (IsBusy)
                return;
            if (!IsBusy)
                IsBusy = true;
            await AddToCartClicked();
            await PopupNavigation.Instance.PopAsync();

        }
        protected override bool OnBackgroundClicked()
        {
            
            return base.OnBackgroundClicked();
        }
        protected override bool OnBackButtonPressed()
        {
            // Return true if you don't want to close this popup page when a back button is pressed
            return base.OnBackButtonPressed();
        }
    }
}