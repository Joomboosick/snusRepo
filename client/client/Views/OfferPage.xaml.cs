﻿using client.Models;
using client.Services;
using Plugin.Geolocator;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace client.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OfferPage : ContentPage
    {
        public ObservableCollection<Item> Items { get; set; }

        public OfferPage(ObservableCollection<Item> items, string fullPrice)
        {
            InitializeComponent();
            var i = ItemsCollectionView.ItemsSource;
            Items = items;
            BindingContext = this;
            dataPicker.MaximumDate = DateTime.Now.AddDays(7);
            dataPicker.MinimumDate = DateTime.Now.AddDays(1);
            OfferButton.Text = fullPrice.Replace(fullPrice.Split('\n')[0],"Подтвердить заказ!");
            OfferButton.Clicked += OfferButton_Clicked;
        }
        Offer offer;
        LoginDataDTO loginData;
        private void OfferButton_Clicked(object sender, EventArgs e)
        {
            if (isLock)
                return;
            isLock = true;

            if (timePicker.Time.Hours > 23 || timePicker.Time.Hours < 17)
            {
                DisplayAlert("Ошибка", "Доставка работает только с 17 до 23 часов", "Ок");
                isLock = false;
                return;
            }
            if (string.IsNullOrEmpty(AdressEntry.Text))
            {
                DisplayAlert("Ошибка", "Введите адрес доставки", "Ок");
                isLock = false;
                return;

            }
            var items = new List<Item>();
            var username = "";
            loginData = App.Current.Properties["LoginData"] as LoginDataDTO;
            username = loginData.username;
            foreach (var item in Items)
            {
                items.Add(item);
            }
            DeliveryInfo deliveryInfo = new DeliveryInfo
            {
                Date = dataPicker.Date.ToString(),
                Time = timePicker.Time.ToString(),
                Items = items.ToArray(),
                FullPrice = OfferButton.Text.Replace("Подтвердить заказ!\n", "")
            };
            offer = new Offer
            {
                clientUserName = username,
                adress = AdressEntry.Text,
                attention = AttentionEntry.Text,
                deliveryInfo = deliveryInfo
            };
            GetAttentionMessage();

        }
        private bool isLock = false;

       

        private async Task GetAttentionMessage()
        {
            string displayMessage = "После определения курьера, вам прийдёт уведомление\n" +
                                    "Курьер свяжется с вами по указанному при регистрации номеру за 15 минут до прибытия\n" +
                                    OfferButton.Text.Replace(OfferButton.Text.Split('\n')[0], "");
            var ans = await DisplayAlert("Внимание", displayMessage, "Прочитал и согласен", "Отказываюсь");
            if (ans)
                SendOffer();
            else
                isLock = false;
            return;
        }
        private void SendOffer()
        {
            HttpClientMy httpClient = new HttpClientMy();
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(offer);
            json = httpClient.HttpPost("offerDelivery.php", json, loginData);
            switch (json)
            {
                case "date":
                    DisplayAlert("Ошибка", "На этот день уже есть заказ от вас!", "Ок");
                    isLock = false;
                    return;
                case "ok":
                    DisplayAlert("Подтверждено", "Заказ оформлен", "Ок");
                    App.CartDatabase.ClearTheCartAsync();
                    MessagingCenter.Send<Page>(this, "GetNotifyStart");
                    Navigation.PopModalAsync();
                    Navigation.PopModalAsync();
                    isLock = false;
                    return;
                default:
                    break;
            }
            isLock = false;

        }
        bool IsGpsLock = false;
        private async void GetGpsButton_Clicked(object sender, EventArgs e)
        {

            if (IsGpsLock)
                return;
            GetGpsButton.IsVisible = false;
            LoadGpsGif.IsVisible = true;
            IsGpsLock = true;
            Plugin.Geolocator.Abstractions.IGeolocator locator = null;
            try
            {
                locator = CrossGeolocator.Current;

                locator.DesiredAccuracy = 50;
                var position = await locator.GetPositionAsync(TimeSpan.FromSeconds(10), null, true);
                var adress = await locator.GetAddressesForPositionAsync(position);
                if (position == null)
                {
                    return;
                }
                List<string> adresses = new List<string>();
                var adressArray = adress.ToArray();
                for (var i = 0; i < 6; i++)
                {
                    if (i == 5)
                    {
                        adresses.Add(position.Latitude.ToString() + " " + position.Longitude.ToString());
                    }
                    if (!string.IsNullOrEmpty(adressArray[i].Thoroughfare)
                       &&
                       !string.IsNullOrEmpty(adressArray[i].SubThoroughfare))
                        adresses.Add(adressArray[i].Thoroughfare + ", " + adressArray[i].SubThoroughfare);
                }
                var asd = await DisplayActionSheet("Выберите адрес(кординаты если ни один не подходит)", null, null, adresses.ToArray());

                AdressEntry.Text = asd;
            }
            catch (Exception)
            {
                await DisplayAlert("ОШИБКА", "Вы запретили приложению использовать gps", "Ок");

                GetGpsButton.IsVisible = true;
                LoadGpsGif.IsVisible = false;
                IsGpsLock = false;
                return;
            }
            GetGpsButton.IsVisible = true;
            LoadGpsGif.IsVisible = false;
            IsGpsLock = false;

        }
    }
}