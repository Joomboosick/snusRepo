﻿using client.Models;
using client.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace client.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AllOfferPage : ContentPage
    {
        public ObservableCollection<Offer> Items { get; set; }
        bool IsMy = false;

        public AllOfferPage()
        {
            InitializeComponent();
            Items = new ObservableCollection<Offer>();
            BindingContext = this;
            Command command = new Command(async () => await RefreshPage());
            MessagingCenter.Subscribe<Page>(this,
                "RefreshAllOffersPage",
                async (sender) => { await RefreshPage(); }
                );
            MyRefreshView.Command = command;
            GetItemsFromServer();
        }
        private async Task RefreshPage()
        {
            MyRefreshView.IsRefreshing = true;
            try
            {
                if (!IsMy)
                    GetItemsFromServer();
                else
                    GetMyItemsFromServer();
            }
            finally
            {

                MyRefreshView.IsRefreshing = false;
            }
        }

        private void GetItemsFromServer()
        {
            Items.Clear();
            HttpClientMy httpClient = new HttpClientMy();
            var json = httpClient.HttpPost("getDelivery.php", "{\"my\":\"false\"}", (LoginDataDTO)App.Current.Properties["LoginData"]);
            if (!string.IsNullOrEmpty(json))
            {
                var getOffers = Newtonsoft.Json.JsonConvert.DeserializeObject<List<getOffer>>(json);
                foreach (var item in getOffers)
                {
                    var dInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<DeliveryInfo>(item.deliveryInfo);
                    Items.Add(new Offer
                    {
                        id = item.id,
                        adress = item.adress,
                        attention = item.attention,
                        Carrier = item.Carrier,
                        clientUserName = item.clientUserName,
                        deliveryInfo = dInfo,
                        status = item.status
                    });
                }
            }
        }
        private void GetMyItemsFromServer()
        {
            Items.Clear();
            HttpClientMy httpClient = new HttpClientMy();
            var json = httpClient.HttpPost("getDelivery.php", "{\"my\":\"false\",\"carrier\":\"true\"}", (LoginDataDTO)App.Current.Properties["LoginData"]);
            if (!string.IsNullOrEmpty(json))
            {
                var getOffers = Newtonsoft.Json.JsonConvert.DeserializeObject<List<getOffer>>(json);
                foreach (var item in getOffers)
                {
                    var dInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<DeliveryInfo>(item.deliveryInfo);
                    Items.Add(new Offer
                    {
                        id = item.id,
                        adress = item.adress,
                        attention = item.attention,
                        Carrier = item.Carrier,
                        clientUserName = item.clientUserName,
                        deliveryInfo = dInfo,
                        status = item.status
                    });
                }
            }
        }

        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            if (IsBusy)
                return;
            if (!IsBusy)
            {
                IsBusy = true;
                try
                {
                    var layout = (BindableObject)sender;
                    var item = (Offer)layout.BindingContext;
                    await Navigation.PushModalAsync(new NavigationPage(new BindDeliveryPage(item, IsMy)));
                }
                finally
                {
                    IsBusy = false;
                }

            }
        }

        private void MyDeliveries_Clicked(object sender, EventArgs e)
        {
            IsMy = true;
            GetMyItemsFromServer();
        }

        private void AllDeliveries_Clicked(object sender, EventArgs e)
        {
            IsMy = false;
            GetItemsFromServer();
        }
    }
}