﻿using client.Models;
using client.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace client.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyOfferDetailPage : ContentPage
    {
        public ObservableCollection<Item> Items { get; set; }
        Offer offer;
        public MyOfferDetailPage(Offer _offer)
        {
            InitializeComponent();
            MessagingCenter.Send<Page>(this, "RefreshOffersPage");
            BindingContext = this;
            offer = _offer;
            Items = new ObservableCollection<Item>();
            ItemsCollectionView.ItemsSource = Items;
            fillFields();
        }

        private void fillFields()
        {
            DateLable.Text = offer.deliveryInfo.Date;
            TimeLable.Text = offer.deliveryInfo.Time;
            AdressLabel.Text = offer.adress;
            PriceLabel.Text = offer.deliveryInfo.FullPrice.Replace("Подтвердить заказ!\n","");
            foreach (var item in offer.deliveryInfo.Items)
            {
                item.count = "Кол-во: " + item.count.Replace("Кол-во: ","");
                Items.Add(item);
            }
            if (offer.attention != "null")
                AttentionLabel.Text = offer.attention;
            else
                AttentionLabel.IsVisible = false;
        }

        private async void CancelButton_Clicked(object sender, EventArgs e)
        {
            var ans = await DisplayAlert("Подтверждение", "Отменить заказ?", "да", "нет");
            if (ans)
            {
                HttpClientMy httpClient = new HttpClientMy();
                var json = httpClient.HttpPost("deleteOffer.php", "{\"id\":\"" + offer.id.ToString() + "\"}", (LoginDataDTO)App.Current.Properties["LoginData"]);
                if (json == "ok")
                {
                    await DisplayAlert("Информация", "Заказ отменён!", "Ок");
                    MessagingCenter.Send<Page>(this, "RefreshOffersPage");
                    await Navigation.PopModalAsync();
                }
                else await DisplayAlert("Ошибка", "Неизвестная ошибка повторите попытку", "Ок");
            }
        }

        private async void ConfirmButton_Clicked(object sender, EventArgs e)
        {
            var ans = await DisplayAlert("Подтверждение", "Подтвердить получение", "да", "нет");
            if (ans)
            {
                HttpClientMy httpClient = new HttpClientMy();
                var json = httpClient.HttpPost("ConfirmOffer.php", "{\"id\":\"" + offer.id.ToString() + "\"}", (LoginDataDTO)App.Current.Properties["LoginData"]);
                if (json == "ok")
                {
                    await DisplayAlert("Информация", "Заказ выполнен!", "Ок");
                    MessagingCenter.Send<Page>(this, "RefreshOffersPage");
                    await Navigation.PopModalAsync();
                }
                else
                    await DisplayAlert("Ошибка", "Неизвестная ошибка повторите попытку", "Ок");
            }
        }
    }
}