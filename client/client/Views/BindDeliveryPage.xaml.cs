﻿using client.Models;
using client.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace client.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BindDeliveryPage : ContentPage
    {
        public ObservableCollection<Item> Items { get; set; }
        Offer offer;
        bool IsMy = false;
        public BindDeliveryPage(Offer _offer, bool _IsMy)
        {
            InitializeComponent();
            IsMy = _IsMy;
            ConfirmButton.IsVisible = true;
            if (IsMy)
                ConfirmButton.Text = "Подтвердить";
            if (!IsMy)
                ConfirmButton.Text = "Присвоить";
            BindingContext = this;
            offer = _offer;
            Items = new ObservableCollection<Item>();
            ItemsCollectionView.ItemsSource = Items;
            fillFields();
        }


        private void fillFields()
        {
            DateLable.Text = offer.deliveryInfo.Date;
            TimeLable.Text = offer.deliveryInfo.Time;
            AdressLabel.Text = offer.adress;
            PriceLabel.Text = offer.deliveryInfo.FullPrice.Replace("Подтвердить заказ!\n", "");
            foreach (var item in offer.deliveryInfo.Items)
            {
                item.count = "Кол-во: " + item.count.Replace("Кол-во: ", "");
                Items.Add(item);
            }
            AttentionLabel.Text = offer.attention;
            
        }

        private async void ConfirmButton_Clicked(object sender, EventArgs e)
        {
            string text = "";
            if (!IsMy)
                text = "Если вы не выполните заказ на вас будут наложены санкции в виде 2ой платы за доставку";
            if (IsMy)
                text = "Если в течении 10 минут не придёт подтверждение со второй стороны ваш рейтин будет декриментирован";
            var ans = await DisplayAlert("Внимание", text, "Подтверждаю", "Отказываюсь");
            HttpClientMy httpClient = new HttpClientMy();
            string json;
            if (IsMy && ans)
            {
                json = httpClient.HttpPost("ConfirmOffer.php", "{\"id\":\"" + offer.id.ToString() + "\"}", (LoginDataDTO)App.Current.Properties["LoginData"]);
                if (json == "ok")
                {
                    await DisplayAlert("Информация", "Доставка подтверждена!", "Ок");
                    MessagingCenter.Send<Page>(this, "RefreshAllOffersPage");
                    await Navigation.PopModalAsync();
                }
                else await DisplayAlert("Ошибка", "Неизвестная ошибка повторите попытку", "Ок");
            }
            if(!IsMy && ans)
            {
                json = httpClient.HttpPost("bindDelivery.php", "{\"id\":\"" + offer.id.ToString() + "\"}", (LoginDataDTO)App.Current.Properties["LoginData"]);
                if (json == "ok")
                {
                    await DisplayAlert("Информация", "Заказ присвоена!", "Ок");
                    MessagingCenter.Send<Page>(this, "RefreshAllOffersPage");
                    await Navigation.PopModalAsync();
                }
                if (json == "binded")
                {
                    await DisplayAlert("Ошибка", "Данный заказ уже присвоен!", "Ок");
                    MessagingCenter.Send<Page>(this, "RefreshAllOffersPage");
                    await Navigation.PopModalAsync();
                }
                else if(json !="ok" && json != "binded")
                    await DisplayAlert("Ошибка", "Неизвестная ошибка повторите попытку", "Ок");
            }
        }

    }
}