﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using client.Models;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace client.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class CartPage : ContentPage
    {
        public Item Item { get; set; }
        public string FullPrice { get; set; }
        public ObservableCollection<Item> Itemss { get; set; }

        public CartPage()
        {
            InitializeComponent();
            Item = new Item
            {
                posName = "Item name",
                describ = "This is an item description."
            };

            Itemss = new ObservableCollection<Item>();
            BindingContext = this;
            FullPrice = "";
            GetFromCart();
        }
        bool firstTime = true;
        private void GetFromCart()
        {
            FullPrice = "0";
            if (!firstTime)
            {
                Itemss.Clear();
            }
            else firstTime = false;

            var items = App.CartDatabase.GetItemsAsync();
            foreach (var item in items)
            {
                item.count = "1";
                bool flag = true;
                for(var i = 0; i < Itemss.Count; i++)
                {
                    if (Itemss[i].describ == item.describ)
                    {
                        Itemss[i].count = (int.Parse(Itemss[i].count) + 1).ToString();
                        flag = false;
                    }
                }
                if (flag)
                    Itemss.Add(item);
            }
            
            foreach (var item in Itemss)
            {
                int fPrice = int.Parse(FullPrice);
                int price = 0;
                if (int.Parse(item.count) >= 10)
                    price = int.Parse(item.optPrice);
                else
                    price = int.Parse(item.price.Replace(" руб.", ""));
                for (int i = 0; i < int.Parse(item.count); i++)
                {
                    fPrice += price;
                }
                FullPrice = fPrice.ToString();
            }
            FullPrice = "Перейти к оформлению заказа\nОбщая стоимость: " + FullPrice + " руб.";
            OfferButton.Text = FullPrice;
        }

        void Clear_Clicked(object sender, EventArgs e)
        {
            App.CartDatabase.ClearTheCartAsync();
            GetFromCart();
        }

        void Incriment_Clicked(object sender, EventArgs e)
        {
            var layout = (BindableObject)sender;
            var item = (Item)layout.BindingContext;
            for (int i = 0; i < Itemss.Count; i++)
            {
                if (Itemss[i].describ == item.describ)
                {
                    Itemss[i].count = (int.Parse(Itemss[i].count) + 1).ToString();
                    App.CartDatabase.SaveItemAsync(item);
                }
            }
            GetFromCart();
        }

        void Decriment_Clicked(object sender, EventArgs e)
        {
            var layout = (BindableObject)sender;
            var item = (Item)layout.BindingContext;
            for (int i = 0; i < Itemss.Count; i++)
            {
                if (Itemss[i].describ == item.describ)
                {
                    Itemss[i].count = (int.Parse(Itemss[i].count) - 1).ToString();
                    App.CartDatabase.ClearTheCartAsync();
                }
            }
            foreach (var itm in Itemss)
            {
                for (int i = 0; i < int.Parse(itm.count); i++)
                {
                    App.CartDatabase.SaveItemAsync(itm);
                }
            }
            GetFromCart();
        }

        async void Cancel_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }

        private async void OfferButton_Clicked(object sender, EventArgs e)
        {
            if(Itemss.Count > 0)
                await Navigation.PushModalAsync(new NavigationPage(new OfferPage(Itemss, FullPrice)));
        }
    }
}