﻿using client.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace client.Services
{
    class HttpClientMy
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="method">mainUrl+method</param>
        /// <param name="jsonData">PostData</param>
        /// <param name="loginData"></param>
        /// <returns></returns>
        public string HttpPost(string method, string jsonData, LoginDataDTO loginData = null)
        {
            HttpClient client = new HttpClient();
            string json;
            if (loginData == null)
            {
                try
                {
                    loginData = (LoginDataDTO)App.Current.Properties["LoginData"];
                }
                catch{}
            }

            string mainUrl = App.Current.Properties["mainUrl"].ToString();
            var logindata = loginData == null ? "" : JsonConvert.SerializeObject(loginData);
            var pairs = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("logindata", logindata),
                new KeyValuePair<string, string>("json", jsonData)
            };
            
            client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.77 YaBrowser/20.11.0.817 Yowser/2.5 Safari/537.36");
            var content = new FormUrlEncodedContent(pairs);
            var response = client.PostAsync(mainUrl + method, content).Result;
            json = response.Content.ReadAsStringAsync().Result;


            return json;
        }
       
    }
}
