﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using client.Models;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace client.Services
{
    public class MockDataStore : IDataStore<Item>
    {
        readonly List<Item> items;
        HttpClientMy http;
        public MockDataStore()
        {
            items =  GetItemsFromServer();
        }

        public List<Item> GetItemsFromServer()
        {
            http = new HttpClientMy();
            var json = http.HttpPost("getPrice.php", "", (LoginDataDTO)App.Current.Properties["LoginData"]);
            var items = JsonConvert.DeserializeObject<Item[]>(json);
            List<Item> outItems = new List<Item>();
            int j = 1;
            foreach (Item item in items)
            {
                item.GUId = Guid.NewGuid().ToString();
                item.price += " руб.";
                outItems.Add(item);
                j++;
            }
            return outItems;
        }
        public async Task<bool> AddItemAsync(Item item)
        {
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemAsync(Item item)
        {
            var oldItem = items.Where((Item arg) => arg.GUId == item.GUId).FirstOrDefault();
            items.Remove(oldItem);
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(string id)
        {
            var oldItem = items.Where((Item arg) => arg.GUId == id).FirstOrDefault();
            items.Remove(oldItem);

            return await Task.FromResult(true);
        }

        public async Task<Item> GetItemAsync(string id)
        {
            return await Task.FromResult(items.FirstOrDefault(s => s.GUId == id));
        }

        public async Task<IEnumerable<Item>> GetItemsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(items);
        }
    }
}