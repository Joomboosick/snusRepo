﻿using client.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace client.Services
{
    public class CartService
    {
        readonly SQLiteAsyncConnection _database;
        public CartService(string dbPath)
        {
            _database = new SQLiteAsyncConnection(dbPath);
            _database.CreateTableAsync<Item>().Wait();
        }

        public List<Item> GetItemsAsync()
        {
            var i = _database.Table<Item>().ToListAsync().Result;
            try
            {
                return i;
            }
            catch (Exception) { }
            return new List<Item>();

        }

        public int SaveItemAsync(Item item)
        {
            return _database.InsertAsync(item).Result;
        }
        public int ClearTheCartAsync()
        {
           return _database.DeleteAllAsync<Item>().Result;
        }
        public Task<int> DeleteItemAsync(int itemId)
        {
            return _database.DeleteAsync<Item>(itemId);
        }
    }
}
