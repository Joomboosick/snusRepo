﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using client.Models;
using SQLite;

namespace client.Services
{
    public class PropertyDatabase
    {
        readonly SQLiteConnection _database;
        public PropertyDatabase(string dbPath)
        {
            _database = new SQLiteConnection(dbPath);
            _database.CreateTable<LocalLoginData>();
        }

        public List<LocalLoginData> GetAccAsync()
        {
            return _database.Table<LocalLoginData>().ToList();
        }

        public int SaveAccAsync(LocalLoginData person)
        {
            return _database.Insert(person);
        }
        public void DeleteAccAsync()
        {
            List<LocalLoginData> asd = GetAccAsync();
            while(asd.Count > 0)
            {
                foreach (var item in asd)
                {
                   _database.Delete<LocalLoginData>(item.ID);
                }
                asd = GetAccAsync();
            }
        }
    }
}
