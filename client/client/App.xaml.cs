﻿using System;
using Xamarin.Forms;
using client.Services;
using client.Views;
using client.Models;
using System.IO;
using System.Collections.Generic;
using DLToolkit.Forms.Controls;

namespace client
{
    public partial class App : Application
    {
        readonly string  mainUrl = "http://joomboosick.ddns.net/";
        public App()
        {
            InitializeComponent();
            
            DependencyService.Register<MockDataStore>();
            FlowListView.Init();
        }
        static PropertyDatabase database;
        static CartService CartDB;


        public static PropertyDatabase PropDatabase
        {
            get
            {
                if (database == null)
                {
                    string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "AccSettings.db3");

                    database = new PropertyDatabase(path);
                }
                return database;
            }
        }

        public static CartService CartDatabase
        {
            get
            {
                if (CartDB == null)
                {
                    CartDB = new CartService(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Cart.db3"));
                }
                return CartDB;
            }
        }
        protected override void OnStart()
        {
            try
            {
                App.Current.Properties.Add("mainUrl", mainUrl);
            }
            catch (Exception) {}

            if (FindAccount())
            {
                if (GetItemsFromServer())
                    MessagingCenter.Send<Page>(new Page(), "GetNotifyStart");
                SetPriveleges();
                    MainPage = new MainPage();
                
            }
            else
                MainPage = new LoginPage();
        }
        private bool GetItemsFromServer()
        {
            
            HttpClientMy httpClient = new HttpClientMy();
            var json = httpClient.HttpPost("getDelivery.php", "{\"my\":\"true\"}", (LoginDataDTO)App.Current.Properties["LoginData"]);
            if (json == "error 403")
                return false;
            if (!string.IsNullOrEmpty(json))
            {
                var getOffers = Newtonsoft.Json.JsonConvert.DeserializeObject<List<getOffer>>(json);
                if (getOffers.Count > 0)
                    return true;
            }
            return false;
        }
        private void SetPriveleges()
        {
            int usertype = (int)App.Current.Properties["UserType"];
            List<HomeMenuItem> HMItemList;
            switch (usertype)
            {
                case (int)UserType.manager:
                    HMItemList = new List<HomeMenuItem>
                    {
                        new HomeMenuItem {Id = MenuItemType.Browse, Title="Позиции" },
                        new HomeMenuItem {Id = MenuItemType.myOffers, Title="Мои заказы" },
                        new HomeMenuItem {Id = MenuItemType.offers, Title="Заказы" },
                        new HomeMenuItem {Id = MenuItemType.stats, Title="Статистика" },
                        new HomeMenuItem {Id = MenuItemType.clients, Title="Клиенты" },
                        new HomeMenuItem {Id = MenuItemType.About, Title="О нас" }
                    };
                    break;
                case (int)UserType.carrier:
                    HMItemList = new List<HomeMenuItem>
                    {
                        new HomeMenuItem {Id = MenuItemType.Browse, Title="Позиции" },
                        new HomeMenuItem {Id = MenuItemType.myOffers, Title="Мои заказы" },
                        new HomeMenuItem {Id = MenuItemType.offers, Title="Заказы" },
                        new HomeMenuItem {Id = MenuItemType.stats, Title="Статистика" },
                        new HomeMenuItem {Id = MenuItemType.About, Title="О нас" }
                    };
                    break;
                case (int)UserType.user:
                    HMItemList = new List<HomeMenuItem>
                    {
                        new HomeMenuItem {Id = MenuItemType.Browse, Title="Позиции" },
                        new HomeMenuItem {Id = MenuItemType.myOffers, Title="Мои заказы" },
                        new HomeMenuItem {Id = MenuItemType.About, Title="О нас" }
                    };
                    break;
                default:
                    HMItemList = new List<HomeMenuItem>
                    {
                        new HomeMenuItem {Id = MenuItemType.Browse, Title="Позиции" },
                        new HomeMenuItem {Id = MenuItemType.myOffers, Title="Мои заказы" },
                        new HomeMenuItem {Id = MenuItemType.About, Title="О нас" }
                    };
                    break;
            }
            App.Current.Properties.Add("HMItemList", HMItemList);
        }
        private bool FindAccount()
        {

            List<LocalLoginData> lld = PropDatabase.GetAccAsync();
            if (lld.Count > 0)
            {
                int userType = 2;
                LoginDataDTO logData = new LoginDataDTO();

                if (lld[0].Password != string.Empty)
                    logData.password = lld[0].Password;
                else return false;

                if (lld[0].Login != string.Empty)
                    logData.username = lld[0].Login;
                else return false;
                logData.login = true;
                userType = lld[0].UserType;
                HttpClientMy httpClient = new HttpClientMy();
                var json = httpClient.HttpPost("auth.php", "", logData);
                if (json == "access denied!")
                    return false;
                logData.login = false;
                App.Current.Properties.Add("UserType", userType);
                App.Current.Properties.Add("LoginData", logData);

                return true;
            }
            else return false;
            
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
