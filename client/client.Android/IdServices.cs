﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using Firebase.Iid;
using Firebase.Messaging;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace client.Droid
{
    [Service(Exported =true, Name = "com.JBSCompany.SnusClient.Id")]
    [IntentFilter(new[] { "com.google.firebase.INSTANCE_ID_EVENT"})]
    class IdServices : FirebaseInstanceIdService
    {
        string ChannelId = "snusNaVashVkus";
        const string TAG = "MyFirebaseIIDService";
        public override void OnCreate()
        {
            base.OnCreate();

            if (FirebaseInstanceId.Instance.Token != null)
                SendRegistrationToServer(FirebaseInstanceId.Instance.Token);
            MessagingCenter.Subscribe<Page>(
                this,
                "RefreshToken",
                (sender) => { SendRegistrationToServer(FirebaseInstanceId.Instance.Token); });
            if (FirebaseInstanceId.Instance.Token != null)
                System.Diagnostics.Debug.WriteLine("InstanceID token: " + FirebaseInstanceId.Instance.Token);
        }
       
        public override void OnTokenRefresh()
        {
            var refreshedToken = FirebaseInstanceId.Instance.Token;
            System.Diagnostics.Debug.WriteLine(TAG, "Refreshed token: " + refreshedToken);
            SendRegistrationToServer(refreshedToken);
        }
        void SendRegistrationToServer(string token)
        {
            string logindata;
            string mainUrl = App.Current.Properties["mainUrl"].ToString();
            try
            {
                global::client.Models.LoginDataDTO loginData = App.Current.Properties["LoginData"] as global::client.Models.LoginDataDTO;
                logindata = loginData == null ? "" : Newtonsoft.Json.JsonConvert.SerializeObject(loginData);
            }
            catch (Exception)
            {
                return;
            }
            HttpClient client = new HttpClient();
            var pairs = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("logindata", logindata),
                new KeyValuePair<string, string>("deviceToken", token)
            };
            client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.77 YaBrowser/20.11.0.817 Yowser/2.5 Safari/537.36");
            var content = new FormUrlEncodedContent(pairs);
            var response = client.PostAsync(mainUrl + "setDeviceToken.php", content).Result;
            


            
        }
        

        
    }
        
}