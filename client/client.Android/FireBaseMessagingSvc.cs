﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using Firebase.Messaging;

namespace client.Droid
{
    [Service(Exported =true,Process ="SnusNotifyCenter",Name = "com.JBSCompany.SnusClient.Notify")]
    [IntentFilter(new[] { "com.google.firebase.MESSAGING_EVENT" })]
    class FireBaseMessagingSvc : FirebaseMessagingService
    {
        public override void OnCreate()
        {
            base.OnCreate();
            

        }
        void CreateNotificationChannel()
        {
            if (Build.VERSION.SdkInt < BuildVersionCodes.O)
            {
                // Notification channels are new in API 26 (and not a part of the
                // support library). There is no need to create a notification
                // channel on older versions of Android.
                return;
            }

            var channelName = "snusChanelNotify";
            var channelDescription = "snusChanelNotify";
            var channel = new NotificationChannel(ChannelId, channelName, NotificationImportance.Default)
            {
                Description = channelDescription
            };

            var notificationManager = (NotificationManager)GetSystemService(NotificationService);
            notificationManager.CreateNotificationChannel(channel);
        }
        public override void OnMessageReceived(RemoteMessage msg)
        {
            var ntf = msg.GetNotification();
            CreateNotificationChannel();
            CreateNotification(ntf.Title,ntf.Body,msg.Data);
        }
        string ChannelId = "snusNaVashVkus";
       
        void CreateNotification(string Title, string text, IDictionary<string, string> data)
        {
            const int notificationId = 0;

            var intent = new Intent(this, typeof(MainActivity));
            intent.AddFlags(ActivityFlags.ClearTop);
            foreach (var key in data.Keys)
            {
                intent.PutExtra(key, data[key]);
            }

            var pendingIntent = PendingIntent.GetActivity(  this,
                                                            notificationId,
                                                            intent,
                                                            PendingIntentFlags.OneShot
                                                            );

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, ChannelId)
                .SetContentTitle(Title)
                .SetContentText(text)
                .SetAutoCancel(true)
                .SetSmallIcon(Resource.Drawable.xamarin_logo)
                .SetContentIntent(pendingIntent)
                .SetDefaults((int)NotificationDefaults.All);

            
            Notification notification = builder.Build();

            
            NotificationManager notificationManager =
                GetSystemService(Context.NotificationService) as NotificationManager;

            
            notificationManager.Notify(notificationId, notification);
        }
    }
}