﻿
using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.OS;
using Android.Gms.Common;
using Firebase.Messaging;
using Firebase.Iid;
using Android.Util;
using Android;
using Android.Content;
using Acr.UserDialogs;
using Xamarin.Forms;
using client.Models;
using System.Collections.Generic;


namespace client.Droid
{
    [Activity(Label = "Снюс на ваш вкус", Icon = "@mipmap/icon", Theme = "@style/Splash", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            SetTheme(Resource.Style.MainTheme);
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);
            //todo: messagingSVC to this action from order 
            if (CheckSelfPermission(Manifest.Permission.AccessCoarseLocation) != (int)Permission.Granted)
            {
                RequestPermissions(new string[] { Manifest.Permission.AccessCoarseLocation, Manifest.Permission.AccessFineLocation }, 0);
            }
            Rg.Plugins.Popup.Popup.Init(this, savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);


            if (Build.VERSION.SdkInt < BuildVersionCodes.OMr1)
            {
                UserDialogs.Init(this);
            }

            XamEffects.Droid.Effects.Init();
            if (Intent.Extras == null)
            {
                var intent = new Intent(this, typeof(IdServices));
                StartService(intent);
                intent = new Intent(this, typeof(FireBaseMessagingSvc));
                StartService(intent);
            }



            MessagingCenter.Subscribe<Page>(
                this,
                "PlayServiceCheck",
                (sender) => { CheckPlayService(); });
            MessagingCenter.Subscribe<Page>(
                this,
                "ExtrasCheck",
                (sender) => { CheckExtras(); });

            LoadApplication(new App());
        }

        //TODO for miui display alert about energy saver
//          Intent intent = new Intent();
//          intent.SetComponent(new ComponentName("com.miui.powerkeeper", "com.miui.powerkeeper.ui.HiddenAppsConfigActivity"));
//          intent.PutExtra("package_name", AppInfo.PackageName);
//          intent.PutExtra("package_label", Resources.GetString(Resource.String.app_name));
//          StartActivity(intent);
//SOLUTION need to binding on messanging center

        private void CheckExtras()
        {
            if (Intent.Extras != null)
            {
                foreach (var key in Intent.Extras.KeySet())
                {
                    var value = Intent.Extras.GetString(key);
                    if (key == "status")
                    {
                        switch (value)
                        {
                            case "1":
                                var menuItems = App.Current.Properties["HMItemList"] as List<HomeMenuItem>;

                                for (int i = 0; i < menuItems.Count; i++)
                                {
                                    if (menuItems[i].Id == MenuItemType.myOffers)
                                    {
                                        App.Current.Properties.Add("MainMenuItem", 2);
                                        App.Current.Properties.Add("SelectedMenuItem", i);
                                        break;
                                    }
                                }

                                return;

                            default:
                                break;
                        }
                    }
                }
            }
        }

        string ChannelId = "snusNaVashVkus";

       

        void CheckPlayService()
        {
            if (!IsPlayServicesAvailable())
            {
                try
                {

                    MessagingCenter.Send<Page>(new Page(), "googlePlaySvcError");
                }
                catch (System.Exception ex)
                {

                    throw;
                }
            }
            
        }
        
        public bool IsPlayServicesAvailable()
        {
            int resultCode = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this);
            if (resultCode != ConnectionResult.Success)
            {
                if (GoogleApiAvailability.Instance.IsUserResolvableError(resultCode))
                    System.Diagnostics.Debug.WriteLine(GoogleApiAvailability.Instance.GetErrorString(resultCode));
                else
                {
                    
                    Finish();
                }
                return false;
            }
            else
            {
                
                return true;
            }
        }


        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}